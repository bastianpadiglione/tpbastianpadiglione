package fr.ensiie.tpbastianpadiglione

import android.app.Application
import fr.ensiie.tpbastianpadiglione.network.Api

class App: Application() {
    override fun onCreate() {
        super.onCreate()
        Api.setUpContext(this)
    }
}