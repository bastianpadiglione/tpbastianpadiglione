package fr.ensiie.tpbastianpadiglione

enum class ActionType {
    CREATION, EDITION
}