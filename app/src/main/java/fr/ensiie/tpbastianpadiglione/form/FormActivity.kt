package fr.ensiie.tpbastianpadiglione.form

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import fr.ensiie.tpbastianpadiglione.ActionType
import fr.ensiie.tpbastianpadiglione.R
import fr.ensiie.tpbastianpadiglione.tasklist.Task
import java.util.*

class FormActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_text)

        val task = intent.getSerializableExtra("task") as? Task
        Log.d("TASK", task.toString())
        if(task != null){
            findViewById<EditText>(R.id.newTitleInput).setText(task.title)
            findViewById<EditText>(R.id.newDescriptionInput).setText(task.description)
        }

        val validButton = findViewById<Button>(R.id.validAddTaskButton)


        validButton.setOnClickListener {
            val title = findViewById<EditText>(R.id.newTitleInput).text.toString()
            val description = findViewById<EditText>(R.id.newDescriptionInput).text.toString()
            val newTask = Task(task?.id ?: UUID.randomUUID().toString(), title, description)
            intent.putExtra("task", newTask)

            if (task != null){
                intent.putExtra("actionType", ActionType.EDITION)
            }else{
                intent.putExtra("actionType", ActionType.CREATION)
            }

            setResult(RESULT_OK, intent)
            finish()
        }
    }
}