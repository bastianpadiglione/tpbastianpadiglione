package fr.ensiie.tpbastianpadiglione.network

import android.util.Log
import fr.ensiie.tpbastianpadiglione.tasklist.Task
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow

class TasksRepository {
    private val tasksWebService = Api.taskWebService

    suspend fun refresh(): List<Task>? {
        // Call HTTP (opération longue):
        val tasksResponse = tasksWebService.getTasks()
        // À la ligne suivante, on a reçu la réponse de l'API:
        if (tasksResponse.isSuccessful) {
            return tasksResponse.body()
            // val fetchedTasks = tasksResponse.body()
            // on modifie la valeur encapsulée, ce qui va notifier ses Observers et donc déclencher leur callback
            //if (fetchedTasks != null) _taskList.value = fetchedTasks
        }
        return null
    }

    suspend fun updateTask(task: Task): Task? {
        val tasksResponse = tasksWebService.update(task)
        return if (tasksResponse.isSuccessful) {
            Log.d("UPDATE", "success")
            /*val updatedTask = tasksResponse.body()
                val oldTask = taskList.value.firstOrNull { it.id == updatedTask?.id }
                if (oldTask != null && updatedTask != null) {
                    _taskList.value = taskList.value - oldTask  + updatedTask
                }*/
            Log.d("UPDATE body", tasksResponse.body().toString())

            tasksResponse.body()
        } else {
            tasksResponse.errorBody()
            Log.d("UPDATE", tasksResponse.errorBody().toString())
            null
        }
    }

    suspend fun createTask(task: Task): Task? {
        val tasksResponse = tasksWebService.createTasks(task)
        return if (tasksResponse.isSuccessful) {
            Log.d("CREATE", tasksResponse.body().toString())
            tasksResponse.body()
        } else {
            tasksResponse.errorBody()
            Log.d("CREATE", tasksResponse.errorBody().toString())
            null
        }
    }

    suspend fun removeTask(task: Task): Boolean {
        val tasksResponse = tasksWebService.delete(task.id)
        return tasksResponse.isSuccessful
    }

}