package fr.ensiie.tpbastianpadiglione.network

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import fr.ensiie.tpbastianpadiglione.tasklist.Task
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class TaskListViewModel : ViewModel() {
    private val tasksRepository = TasksRepository()

    private val _taskList = MutableStateFlow<List<Task>>(emptyList())
    val taskList: StateFlow<List<Task>> = _taskList


    fun loadTasks() {
        viewModelScope.launch {
            val fetchedTasks = tasksRepository.refresh()
            if(fetchedTasks != null){
                Log.d("LOAD", "task fetched")

                _taskList.value = fetchedTasks
            }
        }
    }

    fun deleteTask(task: Task) {
        viewModelScope.launch {
            val removedTask = tasksRepository.removeTask(task)
            if (removedTask) {
                Log.d("DELETE", "task removed")
                _taskList.value = taskList.value - task
            }
        }
    }

    fun addTask(task: Task) {
        viewModelScope.launch {
            val createdTask = tasksRepository.createTask(task)
            if (createdTask != null) {
                Log.d("ADD", "task added")

                _taskList.value = taskList.value + createdTask
            }
        }

    }

    fun editTask(task: Task) {
        viewModelScope.launch {

            val updatedTask = tasksRepository.updateTask(task)
            val oldTask = taskList.value.firstOrNull { it.id == task.id }
            Log.d("UPDATED TASK", updatedTask.toString())
            Log.d("OLD TASK", oldTask.toString())
            if (oldTask != null && updatedTask != null) {
                Log.d("Update", "task updated")

                _taskList.value = taskList.value - oldTask + updatedTask
            }
            Log.d("TASK LIST", taskList.value.toString())
        }

    }

}