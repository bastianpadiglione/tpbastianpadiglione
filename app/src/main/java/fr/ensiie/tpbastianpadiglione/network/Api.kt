package fr.ensiie.tpbastianpadiglione.network

import android.content.Context
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import retrofit2.Retrofit

object Api {

    // constantes qui serviront à faire les requêtes
    private const val BASE_URL = "https://android-tasks-api.herokuapp.com/api/"
   // private var TOKEN = ""//"eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo1NDcsImV4cCI6MTY3MDMzMjU2M30.9whm1PPmuc6PgxEPyBkQXm66dlXCZK1g_PeEzHqpTBw"


    lateinit var appContext: Context

    fun setUpContext(context: Context) {
        appContext = context
    }

    // client HTTP
    private val okHttpClient by lazy {
        OkHttpClient.Builder()
            .addInterceptor { chain ->
                // intercepteur qui ajoute le `header` d'authentification avec votre token:
                val newRequest = chain.request().newBuilder()
                    .addHeader("Authorization", "Bearer ${getToken()}")
                    .build()
                chain.proceed(newRequest)
            }
            .build()
    }

    // sérializeur JSON: transforme le JSON en objets kotlin et inversement
    private val jsonSerializer = Json {
        ignoreUnknownKeys = true
        coerceInputValues = true
    }

    // instance de convertisseur qui parse le JSON renvoyé par le serveur:
    private val converterFactory =
        jsonSerializer.asConverterFactory("application/json".toMediaType())

    // permettra d'implémenter les services que nous allons créer:
    private val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(okHttpClient)
        .addConverterFactory(converterFactory)
        .build()

    val userWebService by lazy {
        retrofit.create(UserWebService::class.java)
    }
    val taskWebService by lazy {
        retrofit.create(TasksWebService::class.java)
    }

    fun getToken() : String?{
       return appContext.getSharedPreferences("App", Context.MODE_PRIVATE).getString("auth_token_key", null)
    }
}