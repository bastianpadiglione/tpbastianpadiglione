package fr.ensiie.tpbastianpadiglione.network

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import fr.ensiie.tpbastianpadiglione.user.UserInfo
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import okhttp3.MultipartBody

class UserInfoViewModel : ViewModel() {
    private val userInfoRepository = UserInfoRepository()

    private val _userInfo = MutableStateFlow<UserInfo?>(null)
    val userInfo : StateFlow<UserInfo?> = _userInfo.asStateFlow()

    fun loadUserInfo() {
        viewModelScope.launch {
            val fetchedUserInfo = userInfoRepository.getInfo()
            if(fetchedUserInfo != null){
                Log.d("LOAD user", fetchedUserInfo.toString())
                _userInfo.value = fetchedUserInfo
            }
            else{
                Log.d("LOAD user", "can't load user info")
            }
        }
    }
    fun editUserInfo(userInfo: UserInfo) {
        viewModelScope.launch {

            val updatedUserInfo = userInfoRepository.updateUserInfo(userInfo)
            Log.d("UPDATED TASK", updatedUserInfo.toString())
            if (updatedUserInfo != null) {
                _userInfo.value = updatedUserInfo
            }
        }

    }

    fun editAvatar(photo: MultipartBody.Part) {
        viewModelScope.launch {

            val updatedAvatar = userInfoRepository.updateAvatar(photo)
            Log.d("UPDATED TASK", updatedAvatar.toString())
            if (updatedAvatar != null) {
                _userInfo.value = updatedAvatar
            }
        }

    }

}