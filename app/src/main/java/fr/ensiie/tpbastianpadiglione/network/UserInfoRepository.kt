package fr.ensiie.tpbastianpadiglione.network

import android.net.Uri
import android.util.Log
import fr.ensiie.tpbastianpadiglione.user.UserInfo
import okhttp3.MultipartBody

class UserInfoRepository {
    private val userWebService = Api.userWebService

    suspend fun getInfo(): UserInfo? {
        val userInfoResponse = userWebService.getInfo()
        if (userInfoResponse.isSuccessful) {
            return userInfoResponse.body()
        }
        return null
    }

    suspend fun updateUserInfo(userInfo: UserInfo): UserInfo? {
        Log.d("Update parameter body", userInfo.toString())
        val userInfoResponse = userWebService.update(userInfo)
        return if (userInfoResponse.isSuccessful) {
            Log.d("UPDATE", "success")
            Log.d("UPDATE body", userInfoResponse.body().toString())
            userInfoResponse.body()
        } else {
            Log.d("UPDATE error", userInfoResponse.errorBody().toString())
            null
        }
    }

    suspend fun updateAvatar(photo: MultipartBody.Part): UserInfo? {
        Log.d("Update parameter body", photo.toString())
        val userInfoResponse = userWebService.updateAvatar(photo)
        return if (userInfoResponse.isSuccessful) {
            Log.d("UPDATE", "success")
            Log.d("UPDATE body", userInfoResponse.body().toString())
            userInfoResponse.body()
        } else {
            Log.d("UPDATE error", userInfoResponse.errorBody().toString())
            null
        }
    }


}