package fr.ensiie.tpbastianpadiglione.network

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import coil.load
import com.google.android.material.snackbar.Snackbar
import com.google.modernstorage.mediastore.FileType
import com.google.modernstorage.mediastore.MediaStoreRepository
import com.google.modernstorage.mediastore.SharedPrimary
import fr.ensiie.tpbastianpadiglione.R
import fr.ensiie.tpbastianpadiglione.databinding.ActivityUserInfoBinding
import fr.ensiie.tpbastianpadiglione.user.UserInfo
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.toRequestBody


class UserInfoActivity : AppCompatActivity() {
    private lateinit var binding: ActivityUserInfoBinding
    private val userInfoViewModel = UserInfoViewModel()
    private val mediaStore by lazy { MediaStoreRepository(this) }
    private lateinit var photoUri: Uri

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUserInfoBinding.inflate(layoutInflater)
        setContentView(binding.root)
        userInfoViewModel.loadUserInfo()

        lifecycleScope.launch { // on lance une coroutine car `collect` est `suspend`
            userInfoViewModel.userInfo.collect { userInfo ->
                binding.emailUpdateInput.setText(userInfo?.email)
                binding.firstNameUpdateInput.setText(userInfo?.firstName)
                binding.lastNameUpdateInput.setText(userInfo?.lastName)
                binding.imageView.load(userInfo?.avatar) {
                    //transformations(CircleCropTransformation())
                    error(R.drawable.ic_launcher_background)
                }
            }
        }

        binding.takePictureButton.setOnClickListener { launchCameraWithPermission() }

        checkPermissionWRITE_EXTERNAL_STORAGE(this)
        lifecycleScope.launchWhenStarted {
            photoUri = mediaStore.createMediaUri(
                filename = "picture.jpg",
                type = FileType.IMAGE,
                location = SharedPrimary
            ).getOrThrow()
        }

        binding.uploadImageButton.setOnClickListener { launchGalleryWithPermission() }

        binding.editProfileButton.setOnClickListener { editUserInfo() }
    }

    val MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 124

    fun checkPermissionWRITE_EXTERNAL_STORAGE(
        context: Context
    ): Boolean {
        val currentAPIVersion = Build.VERSION.SDK_INT
        return if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    context,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        (context as Activity?)!!,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    )
                ) {
                    /*
                    showDialog(
                        "External storage", context,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    )*/
                } else {
                    ActivityCompat
                        .requestPermissions(
                            (context as Activity?)!!,
                            arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                            MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE
                        )
                }
                false
            } else {
                true
            }
        } else {
            true
        }
    }

    private val cameraPermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { accepted ->
            if (accepted) launchCamera()
            else showExplanation()
        }

    private val galleryPermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { accepted ->
            if (accepted) launchGallery()
            else showExplanation()
        }

    private val cameraLauncher =
        registerForActivityResult(ActivityResultContracts.TakePicture()) { accepted ->
            if (accepted) handleImage(photoUri)
            else Snackbar.make(binding.root, "Échec!", Snackbar.LENGTH_LONG)
        }

    private val galleryLauncher =
        registerForActivityResult(ActivityResultContracts.GetContent()) { uri ->
            if (uri != null) {
                handleImage(uri)
            }
            //else Snackbar.make(binding.root, "Échec!", Snackbar.LENGTH_LONG)}
        }


    private fun launchCameraWithPermission() {
        val camPermission = Manifest.permission.CAMERA
        val permissionStatus = ContextCompat.checkSelfPermission(this, camPermission)
        val isAlreadyAccepted = permissionStatus == PackageManager.PERMISSION_GRANTED
        val isExplanationNeeded = shouldShowRequestPermissionRationale(camPermission)
        when {
            isAlreadyAccepted -> launchCamera()// lancer l'action souhaitée
            isExplanationNeeded -> showExplanation()
            else -> cameraPermissionLauncher.launch(camPermission)
        }
    }

    private fun launchGalleryWithPermission() {
        val galleryPermission = Manifest.permission.READ_EXTERNAL_STORAGE
        val permissionStatus = ContextCompat.checkSelfPermission(this, galleryPermission)
        val isAlreadyAccepted = permissionStatus == PackageManager.PERMISSION_GRANTED
        val isExplanationNeeded = shouldShowRequestPermissionRationale(galleryPermission)
        when {
            isAlreadyAccepted -> launchGallery()// lancer l'action souhaitée
            isExplanationNeeded -> showExplanation()
            else -> galleryPermissionLauncher.launch(galleryPermission)
        }
    }

    private fun showExplanation() {
        // ici on construit une pop-up système (Dialog) pour expliquer la nécessité de la demande de permission
        AlertDialog.Builder(this)
            .setMessage("🥺 On a besoin de la caméra, vraiment! 👉👈")
            .setPositiveButton("Bon, ok") { _, _ -> launchAppSettings() }
            .setNegativeButton("Nope") { dialog, _ -> dialog.dismiss() }
            .show()
    }

    private fun launchAppSettings() {
        // Cet intent permet d'ouvrir les paramètres de l'app (pour modifier les permissions déjà refusées par ex)
        val intent = Intent(
            Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
            Uri.fromParts("package", this.packageName, null)
        )
        // ici pas besoin de vérifier avant car on vise un écran système:
        startActivity(intent)
    }

    private fun handleImage(photoUri: Uri) {
        // afficher l'image dans l'ImageView
        Log.d("USER INFO ACTIVITY", "Photo taked!")
        userInfoViewModel.editAvatar(convert(photoUri))
    }

    private fun launchCamera() {
        // à compléter à l'étape suivante
        Log.d("USER INFO ACTIVITY", "Camera Launched")
        cameraLauncher.launch(photoUri)
    }

    private fun launchGallery() {
        // à compléter à l'étape suivante
        Log.d("USER INFO ACTIVITY", "Galery Launched")
        galleryLauncher.launch("image/*")
    }

    private fun convert(uri: Uri): MultipartBody.Part {
        return MultipartBody.Part.createFormData(
            name = "avatar",
            filename = "temp.jpeg",
            body = this.contentResolver.openInputStream(uri)!!.readBytes().toRequestBody()
        )
    }

    private fun editUserInfo() {
        userInfoViewModel.editUserInfo(
            UserInfo(
                binding.emailUpdateInput.text.toString(),
                binding.firstNameUpdateInput.text.toString(),
                binding.lastNameUpdateInput.text.toString(),
                null
            )
        )
    }

}