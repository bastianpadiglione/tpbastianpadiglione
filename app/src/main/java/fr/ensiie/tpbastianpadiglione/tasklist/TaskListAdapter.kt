package fr.ensiie.tpbastianpadiglione.tasklist

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import fr.ensiie.tpbastianpadiglione.R
import fr.ensiie.tpbastianpadiglione.databinding.FragmentTaskListBinding
import fr.ensiie.tpbastianpadiglione.databinding.ItemTaskBinding


object ItemDiffCallback : DiffUtil.ItemCallback<Task>() {
    // are they the same "entity" ? (usually same id)
    override fun areItemsTheSame(oldItem: Task, newItem: Task) = oldItem.id == newItem.id

    // do they have the same data ? (content)
    override fun areContentsTheSame(oldItem: Task, newItem: Task) =
        oldItem == newItem//oldItem.title == newItem.title && oldItem.description == newItem.description
}

class TaskListAdapter : ListAdapter<Task, TaskListAdapter.TaskViewHolder>(ItemDiffCallback) {

    var onClickDelete: (Task) -> Unit = {}
    var onClickEdit: (Task) -> Unit = {}

    private lateinit var binding: ItemTaskBinding
    // This property is only valid between onCreateView and
// onDestroyView.

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        binding = ItemTaskBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return TaskViewHolder();
    }

    inner class TaskViewHolder : RecyclerView.ViewHolder(binding.root) {
        fun bind(task: Task) {
            binding.taskTitle.text = task.title
            binding.taskDescription.text = task.description;
            binding.deleteTaskButton.setOnClickListener { onClickDelete(task) }
            binding.editTaskButton.setOnClickListener { onClickEdit(task) }
        }
    }

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        holder.bind(currentList[position])
        Log.d("CURRENT LIST", currentList.toString())
    }
}
