package fr.ensiie.tpbastianpadiglione.tasklist

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import coil.load
import coil.transform.CircleCropTransformation
import fr.ensiie.tpbastianpadiglione.ActionType
import fr.ensiie.tpbastianpadiglione.R
import fr.ensiie.tpbastianpadiglione.databinding.FragmentTaskListBinding
import fr.ensiie.tpbastianpadiglione.form.FormActivity
import fr.ensiie.tpbastianpadiglione.network.Api
import fr.ensiie.tpbastianpadiglione.network.TaskListViewModel
import fr.ensiie.tpbastianpadiglione.network.UserInfoActivity
import fr.ensiie.tpbastianpadiglione.network.UserInfoViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch


class TaskListFragment : Fragment() {
    private val taskListViewModel by viewModels<TaskListViewModel>()
    private val userInfoViewModel by viewModels<UserInfoViewModel>()

    private var adapter: TaskListAdapter = TaskListAdapter()

    private var _binding: FragmentTaskListBinding? = null

    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!


    private val formLauncherCreate =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            val newTask = result.data?.getSerializableExtra("task") as? Task
            if (newTask != null) {
                if (result.data?.getSerializableExtra("actionType") == ActionType.CREATION) {
                    taskListViewModel.addTask(newTask)
                }
            }
        }

    private val formLauncherEdit =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            val updatedTask = result.data?.getSerializableExtra("task") as? Task
            if (updatedTask != null) {
                if (result.data?.getSerializableExtra("actionType") == ActionType.EDITION) {
                    taskListViewModel.editTask(updatedTask)
                }
            }
        }

    private val userInfoLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            Log.d("Result userInfo", result.toString())
        }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        Log.d("FRAGMENT", "onCreateView fct")
        _binding = FragmentTaskListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.d("FRAGMENT", "onViewCreated fct")

        if (null == Api.appContext.getSharedPreferences("App", Context.MODE_PRIVATE)
                .getString("auth_token_key", null)
        ) {
            findNavController().navigate(R.id.action_taskListFragment2_to_authenticationFragment)
        }


        binding.recyclerView.layoutManager = LinearLayoutManager(activity)
        binding.recyclerView.adapter = adapter

        taskListViewModel.loadTasks()
        userInfoViewModel.loadUserInfo()

        adapter.onClickDelete = { task ->
            taskListViewModel.deleteTask(task)
        }
        adapter.onClickEdit = { task ->
            val intent = Intent(activity, FormActivity::class.java)
            intent.putExtra("task", task)
            formLauncherEdit.launch(intent)
        }

        binding.logOutButton.setOnClickListener {
            val SHARED_PREF_TOKEN_KEY = "auth_token_key"
            val preferences = activity!!.getSharedPreferences("App", Context.MODE_PRIVATE)
            preferences.edit().remove(SHARED_PREF_TOKEN_KEY).apply()
            findNavController().navigate(R.id.action_taskListFragment2_to_authenticationFragment)
        }

        binding.profilImage.setOnClickListener {
            val intent = Intent(activity, UserInfoActivity::class.java)
            userInfoLauncher.launch(intent)
        }

        binding.addElementButton.setOnClickListener {
            val intent = Intent(activity, FormActivity::class.java)
            formLauncherCreate.launch(intent)
        }

        lifecycleScope.launch { // on lance une coroutine car `collect` est `suspend`
            taskListViewModel.taskList.collect { tasks ->
                Log.d("FRAGMENT", "update list")
                Log.d("updated list", tasks.toString())
                adapter.submitList(tasks)
            }
        }
        lifecycleScope.launch { // on lance une coroutine car `collect` est `suspend`
            userInfoViewModel.userInfo.collect { userInfo ->
                Log.d("FRAGMENT", "update user info")
                Log.d("updated userInfo", userInfo.toString())
                val newText = "${userInfo?.firstName} ${userInfo?.lastName}"
                binding.userInfoTextView.text = newText
                binding.profilImage.load(userInfo?.avatar) {
                    transformations(CircleCropTransformation())
                    error(R.drawable.ic_launcher_background)
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        Log.d("FRAGMENT", "onResume fct")
        userInfoViewModel.loadUserInfo()
    }
}