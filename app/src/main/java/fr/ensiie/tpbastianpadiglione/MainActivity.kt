package fr.ensiie.tpbastianpadiglione

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import fr.ensiie.tpbastianpadiglione.databinding.ActivityMainBinding
import fr.ensiie.tpbastianpadiglione.network.UserInfoViewModel

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        //setContentView(R.layout.activity_main)
       // binding.textViewLogin.setOnCLickListener { ... }
    }
}