package fr.ensiie.tpbastianpadiglione.user

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import fr.ensiie.tpbastianpadiglione.R
import fr.ensiie.tpbastianpadiglione.databinding.FragmentSignupBinding
import fr.ensiie.tpbastianpadiglione.network.Api
import kotlinx.coroutines.launch

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [SignupFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SignupFragment : Fragment() {
    private var _binding: FragmentSignupBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        Log.d("LOGIN FRAGMENT", "onCreateView fct")
        _binding = FragmentSignupBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.signupButton.setOnClickListener {
            tryToSignUp()
        }
    }

    fun tryToSignUp() {
        if (binding.signUpFirstnameField.text.isNotEmpty()
            && binding.signUpLastnameField.text.isNotBlank()
            && binding.signUpEmailField.text.isNotBlank()
            && binding.signUpPasswordField.text.isNotBlank()
            && binding.signUpPasswordConfirmationField.text.isNotBlank()
        ) {
            val signUpForm = SignUpForm(
                binding.signUpFirstnameField.text.toString(),
                binding.signUpLastnameField.text.toString(),
                binding.signUpEmailField.text.toString(),
                binding.signUpPasswordField.text.toString(),
                binding.signUpPasswordConfirmationField.text.toString()
            )

            lifecycleScope.launch {
                val response = Api.userWebService.signUp(signUpForm)

                if (response.isSuccessful) {

                    val SHARED_PREF_TOKEN_KEY = "auth_token_key"
                    val preferences =
                        activity!!.getSharedPreferences("App", Context.MODE_PRIVATE)
                    preferences.edit().putString(SHARED_PREF_TOKEN_KEY, response.body()!!.token)
                        .apply()
                    findNavController().navigate(R.id.action_signupFragment_to_taskListFragment2)

                } else {
                    Toast.makeText(context, "Erreur de connexion", Toast.LENGTH_LONG).show()
                }
            }
        }
    }
}