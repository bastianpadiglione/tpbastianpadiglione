package fr.ensiie.tpbastianpadiglione.user

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import fr.ensiie.tpbastianpadiglione.R

class AuthenticationActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authentication)
    }
}