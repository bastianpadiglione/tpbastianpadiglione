package fr.ensiie.tpbastianpadiglione.user

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import fr.ensiie.tpbastianpadiglione.R
import fr.ensiie.tpbastianpadiglione.databinding.FragmentAuthenticationBinding
import fr.ensiie.tpbastianpadiglione.databinding.FragmentLoginBinding
import fr.ensiie.tpbastianpadiglione.network.Api
import kotlinx.coroutines.launch

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [LoginFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class LoginFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d("LOGIN FRAGMENT", "onCreateView fct")
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.loginButton.setOnClickListener {
            tryToLogin()
        }
    }

    fun tryToLogin() {

        if (binding.loginPasswordField.text.isNotEmpty() && binding.loginEmailField.text.isNotBlank()) {
            val loginForm = LoginForm(
                binding.loginEmailField.text.toString(),
                binding.loginPasswordField.text.toString()
            )

            lifecycleScope.launch {
                val response = Api.userWebService.login(loginForm)

                if(response.isSuccessful && response.body() != null){
                    val SHARED_PREF_TOKEN_KEY = "auth_token_key"
                    val preferences = activity!!.getSharedPreferences("App", Context.MODE_PRIVATE)
                    preferences.edit().putString(SHARED_PREF_TOKEN_KEY, response.body()!!.token).apply()
                    findNavController().navigate(R.id.action_loginFragment_to_taskListFragment2)

                }else{
                    Toast.makeText(context, "Erreur de connexion", Toast.LENGTH_LONG).show()
                }
            }
        }
    }
}